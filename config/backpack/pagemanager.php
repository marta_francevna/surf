<?php

return [
    'admin_controller_class' => 'App\Http\Controllers\Admin\PagesCrudController',

    'page_model_class'       => 'App\Models\Page',
];
