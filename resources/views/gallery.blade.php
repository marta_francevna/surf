@extends('layouts.app')

@section('title', 'SURF')

@section('content')

    <div class="banner-1" style="background: url({{'/images/'.$info->image_slider}})">

    </div>

    <!-- technology-left -->
    <div class="technology">
        <div class="container">
            <div class="col-md-9 technology-left">
                <div class="gallery" id="gallery">

                    <h2 class="w3">Галлерея</h2>
                    <p>{!! $info->desc_gallery !!}</p>
                    <div class="gallery-grids">
                        @foreach($posts as $post)
                        <div class="gallery-grid">
                            <a href="{{route('post.show', [$post->category->slug, $post->slug])}}" class="wow fadeInUp animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
                                <img src="images/posts/{{$post->image}}" alt=" " class="img-responsive zoom-img">
                            </a>
                        </div>
                        @endforeach
                        <div class="clearfix"> </div>
                    </div>

                </div>
            </div>
            @include('layouts.right-bar')
        </div>
    </div>
@endsection