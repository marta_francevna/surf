@component('mail::message')
    # Сообщение от: {{$feedback->name}}

@component('mail::panel')
<p>{{$feedback->message}}</p>
@component('mail::button', ['url' => 'mailto:'.$feedback->email])
    Ответить
@endcomponent
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
