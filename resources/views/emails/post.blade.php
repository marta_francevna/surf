@component('mail::message')
# {{$post->title}}
@component('mail::panel')
<p>{!! str_limit($post->text, $limit = 100, $end = '...') !!}</p>
@component('mail::button', ['url' => 'http://surf-cms.loc/posts/'.$category->slug.'/'.$post->slug])
        Прочитать
@endcomponent
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
