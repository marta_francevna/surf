@extends('layouts.app')

@section('title', 'SURF')

@section('content')


    <div class="banner" style="background: url({{'/images/'.$info->image_slider}})">
        <div class="container">
            <h2>{{$info->slider_name}}</h2>
            <p>{{$info->desc_slider}}</p>
            @foreach($menu as $item)
                @if($item->link == 'contact')
                    <a href="{{route('contact')}}">Подробнее</a>
                @endif
            @endforeach
        </div>
    </div>
    @include('layouts.category-tab')
    <!-- technology-left -->
    <div class="technology">
        <div class="container">
            <div class="col-md-9 technology-left">
                <div class="tech-no">
                    <!-- technology-top -->

                    <div class="tc-ch wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">

                        <div class="tch-img">
                            <a href="{{route('post.show', [$posts[0]->category->slug, $posts[0]->slug])}}"><img
                                        src="images/posts/{{$posts[0]->image}}" class="img-responsive" alt=""></a>
                        </div>

                        <h3>
                            <a href="{{route('post.show', [$posts[0]->category->slug, $posts[0]->slug])}}">{{$posts[0]->title}}</a>
                        </h3>
                        <h6>BY
                            <a href="">{{$user->name}} </a> {{ \Carbon\Carbon::parse($posts[0]->created_at)->format('d.m.y')}}
                            в {{\Carbon\Carbon::parse($posts[0]->created_at)->format('H:i')}}.</h6>
                        <p>{!! str_limit($posts[0]->text, $limit = 600, $end = '...') !!}  </p>
                        <div class="bht1">
                            <a href="{{route('post.show', [$posts[0]->category->slug, $posts[0]->slug])}}">Далее</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <!-- technology-top -->
                    <!-- technology-top -->
                    <div class="w3ls">
                        <div class="col-md-6 w3ls-left wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
                            <div class="tc-ch">
                                <div class="tch-img">
                                    <a href="{{route('post.show', [$posts[1]->category->slug, $posts[1]->slug])}}">
                                        <img src="images/posts/{{$posts[1]->image}}" class="img-responsive" alt=""></a>
                                </div>

                                <h3>
                                    <a href="{{route('post.show', [$posts[1]->category->slug, $posts[1]->slug])}}">{{$posts[1]->title}}</a>
                                </h3>
                                <h6>BY
                                    <a href="{{route('post.show', [$posts[1]->category->slug, $posts[1]->slug])}}">{{$user->name}}</a> {{ \Carbon\Carbon::parse($posts[1]->created_at)->format('d.m.y')}}
                                    в {{\Carbon\Carbon::parse($posts[1]->created_at)->format('H:i')}}.</h6>
                                <p>{!!  str_limit($posts[1]->text, $limit = 200, $end = '...')  !!}</p>
                                <div class="bht1">
                                    <a href="{{route('post.show', [$posts[1]->category->slug, $posts[1]->slug])}}">Далее</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 w3ls-left wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
                            <div class="tc-ch">
                                <div class="tch-img">
                                    <a href="{{route('post.show', [$posts[2]->category->slug, $posts[2]->slug])}}"><img
                                                src="images/posts/{{$posts[2]->image}}" class="img-responsive"
                                                alt=""></a>
                                </div>

                                <h3>
                                    <a href="{{route('post.show', [$posts[2]->category->slug, $posts[2]->slug])}}">{{$posts[2]->title}}</a>
                                </h3>
                                <h6>BY
                                    <a href="{{route('post.show', [$posts[2]->category->slug, $posts[2]->slug])}}">{{$user->name}}  </a> {{ \Carbon\Carbon::parse($posts[2]->created_at)->format('d.m.y')}}
                                    в {{\Carbon\Carbon::parse($posts[2]->created_at)->format('H:i')}}.</h6>
                                <p>{!! str_limit($posts[2]->text, $limit = 200, $end = '...') !!} </p>
                                <div class="bht1">
                                    <a href="{{route('post.show', [$posts[2]->category->slug, $posts[2]->slug])}}">Далее</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- technology-top -->
                    <div class="wthree">
                        <div class="col-md-6 wthree-left wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
                            <div class="tch-img">
                                <a href="{{route('post.show', [$posts[3]->category->slug, $posts[3]->slug])}}"><img
                                            src="images/posts/{{$posts[3]->image}}" class="img-responsive" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-6 wthree-right wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
                            <h3>
                                <a href="{{route('post.show', [$posts[3]->category->slug, $posts[3]->slug])}}">{{$posts[3]->title}}</a>
                            </h3>
                            <h6>BY
                                <a href="{{route('post.show', [$posts[3]->category->slug, $posts[3]->slug])}}">{{$user->name}} </a> {{ \Carbon\Carbon::parse($posts[3]->created_at)->format('d.m.y')}}
                                в {{\Carbon\Carbon::parse($posts[3]->created_at)->format('H:i')}}.</h6>
                            <p>{!! str_limit($posts[3]->text, $limit = 100, $end = '...') !!} </p>
                            <div class="bht1">
                                <a href="{{route('post.show', [$posts[3]->category->slug, $posts[3]->slug])}}">Далее</a>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="wthree">
                        <div class="col-md-6 wthree-left wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
                            <div class="tch-img">
                                <a href="{{route('post.show', [$posts[4]->category->slug, $posts[4]->slug])}}"><img
                                            src="images/posts/{{$posts[4]->image}}" class="img-responsive" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-6 wthree-right wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
                            <h3>
                                <a href="{{route('post.show', [$posts[4]->category->slug, $posts[4]->slug])}}">{{$posts[4]->title}}</a>
                            </h3>
                            <h6>BY
                                <a href="{{route('post.show', [$posts[4]->category->slug, $posts[4]->slug])}}">{{$user->name}} </a> {{ \Carbon\Carbon::parse($posts[4]->created_at)->format('d.m.y')}}
                                в {{\Carbon\Carbon::parse($posts[4]->created_at)->format('H:i')}}.</h6>
                            <p> {!! str_limit($posts[4]->text, $limit = 100, $end = '...') !!} </p>
                            <div class="bht1">
                                <a href="{{route('post.show', [$posts[4]->category->slug, $posts[4]->slug])}}">Далее</a>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="wthree">
                        <div class="col-md-6 wthree-left wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
                            <div class="tch-img">
                                <a href="{{route('post.show', [$posts[5]->category->slug, $posts[5]->slug])}}"><img
                                            src="images/posts/{{$posts[5]->image}}" class="img-responsive" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-6 wthree-right wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
                            <h3>
                                <a href="{{route('post.show', [$posts[5]->category->slug, $posts[5]->slug])}}">{{$posts[5]->title}}</a>
                            </h3>
                            <h6>BY
                                <a href="{{route('post.show', [$posts[5]->category->slug, $posts[5]->slug])}}">{{$user->name}}</a> {{ \Carbon\Carbon::parse($posts[5]->created_at)->format('d.m.y')}}
                                в {{\Carbon\Carbon::parse($posts[5]->created_at)->format('H:i')}}.</h6>
                            <p>{!! str_limit($posts[5]->text, $limit = 100, $end = '...') !!} </p>
                            <div class="bht1">
                                <a href="{{route('post.show', [$posts[5]->category->slug, $posts[5]->slug])}}">Далее</a>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            @include('layouts.right-bar')
        </div>
    </div>


@endsection