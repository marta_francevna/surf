@extends('layouts.app')

@section('title', 'SURF')

@section('content')

    <div class="banner-1" style="background: url({{'/images/'.$info->image_slider}})">

    </div>

    <!-- technology-left -->
    <div class="technology">
        <div class="container">
            <div class="col-md-9 technology-left">
                <div class="blog">

                    <h2 class="w3"> Поиск по запросу: {{$search}}</h2>
                    @if(count($posts)>0)
                        @php($i = 1)
                        @foreach($posts as $post)
                            @if ($i%2!=0)
                                <div class="blog-grids1">
                                    @endif
                                    <div class="col-md-6 blog-grid">
                                        <div class="blog-grid-left1">
                                            <a href="{{route('post.show', [$post->category->slug, $post->slug])}}"><img
                                                        src="{{asset('images/posts/'.$post->image)}}" alt=" "
                                                        class="img-responsive"></a>
                                        </div>
                                        <div class="blog-grid-right1">
                                            <a href="{{route('post.show', [$post->category->slug, $post->slug])}}">{{$post->title}}</a>
                                            <h4>{{ \Carbon\Carbon::parse($post->created_at)->format('d.m.y')}}</h4>
                                            <p>{!! str_limit($post->text, $limit = 100, $end = '...')  !!}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="more m1">
                                            <a type="button"
                                               href="{{route('post.show', [$post->category->slug, $post->slug])}}"
                                               class="btn btn-1 btn-default">Далее</a>
                                        </div>
                                    </div>
                                    @if ($i%2==0 || ($i%2!=0 && $i==count($posts)))
                                        <div class="clearfix"></div>
                                </div>
                                    @endif
                            @php($i++)

                        @endforeach
                    @else
                        <div class="help-block">
                            <p>Извините, ничего не найдено:(</p>
                        </div>
                    @endif

                </div>
            </div>
            @include('layouts.right-bar')
            {{--<nav class="paging">--}}
                {{--{{ $posts->links() }}--}}
            {{--</nav>--}}
        </div>

    </div>



@endsection