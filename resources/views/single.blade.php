@extends('layouts.app')

@section('title', 'SURF')

@section('content')
    <div class="banner-1" style="background: url({{'/images/'.$info->image_slider}})">

    </div>

    <!-- technology-left -->
    <div class="technology">
        <div class="container">
            <div class="col-md-9 technology-left">
                <div class="agileinfo">

                    <h2 class="w3">{{$post->title}}</h2>
                    <div class="single">
                        <img src="{{asset('images/posts/'.$post->image)}}" class="img-responsive" alt="">
                        <div class="b-bottom">
                            <h5 class="top">{{$post->title}}</h5>
                            <p class="sub">{!! $post->text !!}</p>
                            <p>{{ \Carbon\Carbon::parse($post->created_at)->format('d.m.y')}}
                                <a class="span_link" ><span class="glyphicon glyphicon-comment"></span>{{$post->comments_count}} </a>
                            </p>

                        </div>
                    </div>


                    @if(count($comments)>0)
                    <div class="response">
                        <h4>Комментарии</h4>
                        @foreach($comments as $comment)
                        <div class="media response-info">

                            <div class="media-left response-text-left">
                                <a href="#">
                                    <img src="{{ \Laravolt\Avatar\Facade::create($comment->name)}}"  alt="">
                                </a>
                            </div>
                            <div class="media-body response-text-right">
                                <ul>
                                    <li><p class="nickname">{{$comment->name}}</li>
                                    <li class="date">{{ \Carbon\Carbon::parse($comment->created_at)->format('d.m.y H:i')}}</li>
                                </ul>
                                <p>{{$comment->message}}</p>
                            </div>
                                <div class="clearfix"> </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                    <div class="coment-form">
                        <h4>Добавьте комментарий</h4>
                        <form method="post" action="{{ route('post.comment', [$post->id], false) }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <input type="text"  name="name" placeholder="Имя" required/>
                            <textarea name="message" required placeholder="Комментарий..."></textarea>
                            <input type="submit" value="Отправить">
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            @include('layouts.right-bar')
        </div>
    </div>


@endsection