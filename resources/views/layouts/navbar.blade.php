<div class="header_right wow fadeInLeft animated animated" data-wow-delay=".5s"
     style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
    <nav class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
            <nav class="link-effect-7" id="link-effect-7">
                <ul class="nav navbar-nav">
                    {{--<li class="active act">--}}
                    @foreach($menu as  $item)
                        @if(isset($item->page))
                            <li class="{{Request::is('page/'.$item->page->slug)?'active act':''}}">
                                <a href="/page/{{$item->page->slug}}">{{$item->name}}</a>
                            </li>
                        @elseif(isset($item->category))
                            <li class="{{Request::is('posts/'.$item->category->slug)?'active act':''}}">
                                <a href="/posts/{{$item->category->slug}}">{{$item->name}}</a>
                            </li>
                        @elseif($item->type == 'internal_link')
                            <li class="{{Request::is($item->link)?'active act':''}}">
                                <a href="/{{$item->link}}">{{$item->name}}</a>
                            </li>
                            @else
                            <li class="{{Request::is($item->link)?'active act':''}}">
                                <a href="{{$item->link}}">{{$item->name}}</a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </nav>
        </div>
    </nav>
</div>