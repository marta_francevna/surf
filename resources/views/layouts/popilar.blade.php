<h4>Популярное</h4>
@foreach($topPosts as $post)
    <div class="blog-grids wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
        <div class="blog-grid-left">
            <a href="{{route('post.show', [$post->category->slug, $post->slug])}}"><img
                        src="{{asset('images/posts/'.$post->image)}}" class="img-responsive" alt=""></a>
        </div>
        <div class="blog-grid-right">

            <h5><a href="{{route('post.show', [$post->category->slug, $post->slug])}}">{{$post->title}}</a></h5>
        </div>
        <div class="clearfix"></div>
    </div>
@endforeach