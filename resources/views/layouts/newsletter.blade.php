<div class="col-md-4 footer-right wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
    <h4>Подписка</h4>
    <p>Оставив свой email, вы подпишетесь на новостную рассылку. </p>
    <div class="name">
        <form action="{{ route('newsletter') }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <input type="text" name="name" placeholder="Имя" required>
            <input type="email" name="email" placeholder="Email" required>
            <input type="submit" value="Подписаться">
        </form>

    </div>

    <div class="clearfix"></div>

</div>