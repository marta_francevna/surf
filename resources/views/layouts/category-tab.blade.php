<div class="services w3l wow fadeInDown"  data-wow-duration=".8s" data-wow-delay=".2s">
    <div class="container">
        <div class="grid_3 grid_5">
            <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="myTab" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#category1" id="category1-tab" role="tab" data-toggle="tab" aria-controls="category1" aria-expanded="true">{{$categoryTabs[0][0]->category->name}}</a></li>
                    <li role="presentation" class=""><a href="#category2" role="tab" id="category2-tab" data-toggle="tab" aria-controls="category2">{{$categoryTabs[1][0]->category->name}}</a></li>
                    <li role="presentation" class=""><a href="#category3" role="tab" id="category3-tab" data-toggle="tab" aria-controls="category3">{{$categoryTabs[2][0]->category->name}}</a></li>
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade  active in" id="category1" aria-labelledby="category1-tab">

                        @foreach($categoryTabs[0] as $category)
                        <div class="col-md-4 col-sm-5 tab-image">
                            <a href="{{route('post.show', [$category->category->slug, $category->slug])}}"> <img src="{{asset('images/posts/'.$category->image)}}" class="img-responsive" alt="Wanderer"></a>
                        </div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="category2" aria-labelledby="category2-tab">
                        @foreach($categoryTabs[1] as $category)
                            <div class="col-md-4 col-sm-5 tab-image">
                                <a href="{{route('post.show', [$category->category->slug, $category->slug])}}">
                                <img src="{{asset('images/posts/'.$category->image)}}" class="img-responsive" alt="Wanderer">
                            </a>
                            </div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="category3" aria-labelledby="category3-tab">

                        @foreach($categoryTabs[2] as $category)
                            <div class="col-md-4 col-sm-5 tab-image">
                                <a href="{{route('post.show', [$category->category->slug, $category->slug])}}"><img src="{{asset('images/posts/'.$category->image)}}" class="img-responsive" alt="Wanderer"></a>

                            </div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>