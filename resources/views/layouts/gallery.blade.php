<!-- technology-right -->
<div class="insta wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
    <h4>Галерея</h4>
    <ul>
        @foreach($gallery as $item)
            <li><a href="{{route('post.show', [$item->category->slug, $item->slug])}}"><img
                            src="{{asset('images/posts/'.$item->image)}}" class="img-responsive" alt=""></a></li>
        @endforeach
    </ul>
</div>
