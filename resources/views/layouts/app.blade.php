<!doctype html>
<html class="no-js" lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script type="applijewelleryion/x-javascript">
         addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); }




    </script>
    <link href="{{asset('css/bootstrap.css')}}" rel='stylesheet' type='text/css'/>
    <!-- Custom Theme files -->
    <link href='//fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>
    <link href="{{asset('css/style.css')}}" rel='stylesheet' type='text/css'/>
    <script src="{{asset('js/jquery-1.11.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="https://use.fontawesome.com/97ffd339fe.js"></script>
    <!-- animation-effect -->
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet">
    <script src="{{asset('js/wow.min.js')}}"></script>
    <script>
        new WOW().init();
    </script>
</head>
<body>
<div class="header" id="ban">
    <div class="container">
        <div class="head-left wow fadeInLeft animated animated" data-wow-delay=".5s"
             style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
            <a class="navbar-brand" rel="home" href="/">
                <img style="width:100px; margin-top: -34px;"
                     src="{{asset('images/'.$info->logo)}}">
            </a>
        </div>
        @include('layouts.navbar')
    </div>
</div>
<!--start-main-->
<div class="header-bottom">
    <div class="container">
        <div class="logo wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
            <h1><a href="/">{{\Illuminate\Support\Str::upper($info->name)}}</a></h1>
            <p>{{$info->quote}}</p>
        </div>
    </div>
</div>
<!-- banner -->
@yield('content')

<div class="footer">
    <div class="container">
        <div class="col-md-4 footer-left wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
            <h4>Обо мне</h4>

            <p>{{$user->about}}</p>
            <img src="{{asset('images/'.$user->image)}}" class="img-responsive" alt="">
            @foreach($menu as $item)
                @if($item->link == 'contact')
                <div class="bht1">
                    <a href="{{route('contact')}}">Подробнее</a>
                </div>
                @endif
            @endforeach
        </div>
        <div class="col-md-4 footer-middle wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
            <h4>Я в соцсетях</h4>
            <div class="soci" style="color: white; float: left">
                <ul>
                    @foreach($social as $item)
                        <li class="hvr-rectangle-out">
                            <a href="{{$item->link}}">
                                <i style="padding: 7px; color: white"
                                   class="fa {{$item->icon}}" aria-hidden="true">
                                </i>
                            </a>
                        </li>
                    @endforeach
                </ul>

            </div>

        </div>
        @include('layouts.newsletter')
        <div class="clearfix"></div>
    </div>
</div>
<div class="copyright wow fadeInDown" data-wow-duration=".8s" data-wow-delay=".2s">
    <div class="container">
        <p>© 2018 S.U.R.F CMS</p>
        <div class="row">
            <div class="">
                @if(!Auth::check())
                    <div class="right-header">
                        <ul>
                            <li><a href="/admin/login" class="active"><i class="fa fa-sign-in"></i>Войти</a>
                            </li>
                        </ul>
                    </div>
                @else
                    <div class="right-header">
                        <ul>
                            <li><a href="{{route('logout')}}" class="active" onclick="event.preventDefault();
                                document.getElementById('logout').submit();"><i class="fa fa-sign-out"></i>Выйти</a>
                            </li>

                            <form id="logout" action="{{ route('logout') }}" method="POST">
                                {{ csrf_field() }}
                            </form>

                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
</body>
</html>
