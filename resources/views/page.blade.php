@extends('layouts.app')

@section('title', 'SURF')

@section('content')

    <div class="banner-1" style="background: url({{'/images/'.$info->image_slider}})">

    </div>

    <!-- technology-left -->
    <div class="technology">
        <div class="container">
            <div class="col-md-9 technology-left">
                <div class="w3agile-1">
                    <div class="welcome">
                        <div class="welcome-top heading">
                        <h2 class="w3">{{$page->name}}</h2>
                        {!! $page->content !!}
                    </div>
                </div>
            </div>
        </div>
            @include('layouts.right-bar')
    </div>
    </div>


@endsection