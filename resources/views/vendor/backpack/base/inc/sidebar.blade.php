@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            {{--<img src="{{ backpack_avatar_url(Auth::user()) }}" class="img-circle" alt="User Image">--}}
            <img src="{{asset('images/admin.png')}}" class="img-circle" alt="User Image">

          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            <small><small><a href="{{ route('backpack.account.info') }}"><span><i class="fa fa-user-circle-o"></i> Мой аккаунт</span></a> &nbsp;  &nbsp; <a href="{{ backpack_url('logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></small></small>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          {{-- <li class="header">{{ trans('backpack::base.administration') }}</li> --}}
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
            <li><a href="{{backpack_url('page') }}"><i class="fa fa-file-o"></i> <span> Страницы</span></a></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-newspaper-o"></i> <span>Блог</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="{{ backpack_url('post') }}"><i class="fa fa-newspaper-o"></i> <span>Посты</span></a></li>
                <li><a href="{{ backpack_url('category') }}"><i class="fa fa-list"></i> <span>Категории</span></a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-cogs"></i> <span>Настройки</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="{{ route('settings') }}"><i class="fa fa-files-o"></i> <span>Сайт</span></a></li>
                <li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/menu-item') }}"><i class="fa fa-list"></i> <span>Меню</span></a></li>
                <li><a href="{{ route('user') }}"><i class="fa fa-user"></i> <span>Владелец</span></a></li>
                <li><a href="{{backpack_url('social') }}"><i class="fa fa-globe"></i> <span>Социальные сети</span></a></li>
              </ul>
            </li>
            <li><a href="{{backpack_url('feedback') }}"><i class="fa fa-group"></i> <span> Обратная связь</span></a></li>
            <li><a href="{{backpack_url('newsletter') }}"><i class="fa fa-envelope"></i> <span> Подписка </span></a></li>
            <li><a href="{{backpack_url('comments') }}"><i class="fa fa-comments"></i> <span> Комментарии </span></a></li>
          <!-- ======================================= -->
          {{-- <li class="header">Other menus</li> --}}
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
