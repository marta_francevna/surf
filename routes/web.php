<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', [
        'as'   => 'index',
        'uses' => 'HomeController@index',
    ]
);


Route::get('/contact', [
        'as'   => 'contact',
        'uses' => 'HomeController@contact',
    ]
);

Route::get('/gallery', [
        'as'   => 'gallery',
        'uses' => 'HomeController@gallery',
    ]
);


Route::get('/posts/{category}', [
        'as'   => 'posts.index',
        'uses' => 'PostController@index',
    ]
);

Route::get('/page/{slug}', [
        'as'   => 'page',
        'uses' => 'PageController@index',
    ]
);

Route::get('/search', [
        'as'   => 'posts.search',
        'uses' => 'PostController@search',
    ]
);

Route::get('/posts/{category}/{slug}', [
        'as'   => 'post.show',
        'uses' => 'PostController@show',
    ]
)->where('slug', '[A-Za-z0-9-_]+');

Route::post('/contact/feedback', [
        'as'   => 'contact.feedback',
        'uses' => 'HomeController@feedback',
    ]
);

Route::post('/newsletter', [
        'as'   => 'newsletter',
        'uses' => 'HomeController@newsletter',
    ]
);

Route::post('/post/comment/{id}', [
    'as'   => 'post.comment',
    'uses' => 'PostController@comment',
]);

 Route::group(['prefix' => 'admin',
               'middleware' => ['admin'],
               'namespace' => 'Admin'], function()
 {
     CRUD::resource('menu-item', 'MenuItemCrudController');
     CRUD::resource('post', 'PostCrudController');
     CRUD::resource('category', 'CategoryCrudController');
//     CRUD::resource('info', 'InfoCrudController');
     Route::get('/settings', [
         'as'   => 'settings',
         'uses' => 'InfoCrudController@edit',
     ]);
     Route::put('/info/1', [
         'uses' => 'InfoCrudController@update',
     ]);

     Route::get('/user', [
         'as'   => 'user',
         'uses' => 'UserCrudController@edit',
     ]);
     Route::put('/user/1', [
         'uses' => 'UserCrudController@update',
     ]);

     CRUD::resource('feedback', 'FeedbackCrudController');
     CRUD::resource('comments', 'CommentsCrudController');
     CRUD::resource('newsletter', 'NewsletterCrudController');
     CRUD::resource('social', 'SocialCrudController');
     Route::get('/edit-account-info', [
         'as'   => 'backpack.account.info',
         'uses' => 'MyEditAccountController@getAccountInfoForm',
     ]);
     Route::post('/edit-account-info', [
         'as'   => 'backpack.account.edit.info',
         'uses' => 'MyEditAccountController@postAccountInfoForm',
     ]);
     Route::get('/change-password', [
         'as'   => 'backpack.account.password',
         'uses' => 'MyEditAccountController@getChangePasswordForm',
     ]);
     Route::post('/change-password', [
         'as'   => 'backpack.account.edit.password',
         'uses' => 'MyEditAccountController@postChangePasswordForm',
     ]);

});





