<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

/**
 * App\Models\Newsletter
 *
 * @mixin \Eloquent
 */
class Newsletter extends Model
{
    use SoftDeletes;
    use CrudTrait;
    use Notifiable;
    use CrudTrait;

    protected $table = 'newsletter';
    protected $fillable = ['name', 'email'];
}
