<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

/**
 * App\Models\Post
 *
 * @mixin \Eloquent
 */
class Post extends Model
{
    use Sluggable, SluggableScopeHelpers;
    use CrudTrait;

    protected $table = 'post';

    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = ['category_id', 'title', 'slug', 'image', 'text', 'status'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_title'
            ]
        ];
    }

    public function getSlugOrTitleAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }

        return $this->title;
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function comments()
    {
        return $this->hasMany(Comments::class);
    }

    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $disk = "public_folder";
        $destination_path = "images/posts";


        if (starts_with($value, 'data:image'))
        {

            $image = Image::make($value);
            $image->resize(750, 450);

            $filename = md5($value.time()).'.jpg';

            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());

            $this->attributes[$attribute_name] = $filename;
        }
    }

}
