<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
/**
 * App\Models\Comments
 *
 * @mixin \Eloquent
 */
class Comments extends Model
{
    use CrudTrait;
    protected $fillable = ['name','message', 'post_id'];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function getOpenButton()
    {
        $post = Post::find($this->post_id);
        $category = Category::find($post->category_id);
        return '<a class="btn btn-default btn-xs" href="'.url('/posts/'.$category->slug.'/'.$post->slug).'" target="_blank">'.
            '<i class="fa fa-eye"></i> Посмотреть</a>';
    }
}
