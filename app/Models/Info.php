<?php

namespace App\Models;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

/**
 * App\Models\Info
 *
 * @mixin \Eloquent
 */
class Info extends Model
{
    use CrudTrait;
    protected $table = 'info';
    protected $fillable = ['name', 'logo', 'quote', 'slider_name','image_slider', 'desc_slider', 'desc_gallery', 'desc_contact'];


    public function setLogoAttribute($value)
    {
        $attribute_name = "logo";
        $disk = "public_folder";
        $destination_path = "images";


        if (starts_with($value, 'data:image'))
        {

            $image = Image::make($value);
            $image->resize(600, 500);

            $filename = md5($value.time()).'.jpg';

            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());

            $this->attributes[$attribute_name] = $filename;
        }
    }

    public function setImageSliderAttribute($value)
    {
        $attribute_name = "image_slider";
        $disk = "public_folder";
        $destination_path = "images";


        if (starts_with($value, 'data:image'))
        {

            $image = Image::make($value);
            $image->resize(1366, 900);

            $filename = md5($value.time()).'.jpg';

            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());

            $this->attributes[$attribute_name] = $filename;
        }
    }
}
