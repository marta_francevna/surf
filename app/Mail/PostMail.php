<?php

namespace App\Mail;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostMail extends Mailable
{
    use Queueable, SerializesModels;

    public $post;
    public $user;

    /**
     * PostMail constructor.
     * @param Post $post
     * @param User $user
     */
    public function __construct(Post $post, User $user)
    {
        $this->post = $post;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->user->email)
            ->subject('Новый пост от '. $this->user->name)
            ->with([
                'category' => Category::find($this->post->category_id)
            ])
        ->markdown('emails.post');
    }
}
