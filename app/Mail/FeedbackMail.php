<?php

namespace App\Mail;

use App\Models\Feedback;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackMail extends Mailable
{
    use Queueable, SerializesModels;

    public $feedback;
    public $user;

    /**
     * FeedbackMail constructor.
     * @param Feedback $feedback
     * @param User $user
     */
    public function __construct(Feedback $feedback, User $user)
    {
        $this->feedback = $feedback;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->user->email)
            ->subject('Новое сообщение обратной связи')
            ->markdown('emails.feedback');
    }
}
