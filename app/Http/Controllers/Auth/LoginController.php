<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\MenuItem;
use App\Models\Social;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use App\Models\Info;
use App\Models\Menu;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    protected $info;
    protected $menu;
    protected $gallery;
    protected $user;
    protected $topPosts;
    protected $categories;
    protected $social;

    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->social = Social::all();
        $this->info = Info::first();
        $this->menu = MenuItem::with('page','category')->orderBy('lft')->get();
        $this->user = User::first();
        $this->gallery = Post::with('category')->inRandomOrder()->limit(9)->where('status','Опубликовано')->get();
        $this->topPosts = Post::with('category')->withCount('comments')->with('comments')->where('status','Опубликовано')->orderBy('comments_count','DESC')->limit(5)->get();
        $this->categories = Category::inRandomOrder()->limit(3);
        \View::share([
            'info'     => $this->info, 'menu' => $this->menu,
            'gallery'  => $this->gallery, 'user' => $this->user,
            'topPosts' => $this->topPosts, 'categories' => $this->categories,
            'social' =>$this->social
        ]);
    }
//
//    public function showLogin(){
//
//        return view('auth.login');
//    }

}
