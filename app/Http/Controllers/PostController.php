<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Comments;
use App\Models\Post;
use Illuminate\Http\Request;


class PostController extends Controller
{
    /**
     * Категория
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($slug)
    {
        $category = Category::where('slug', $slug)->first();
        if ($category) {
            $posts = Post::whereHas('category', function($query) use ($slug) {
                $query->where('slug', $slug);
            })->where('status','Опубликовано')->latest('created_at')->paginate(6);
        }


        return $category ? view('category', compact('posts', 'category'))
            : abort(404);
    }

    /**
     * Пост
     *
     * @param $category
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($category, $slug)
    {
        $post = Post::where('slug', $slug)->with('category')->whereHas('category', function($query) use ($category) {
            $query->where('slug', $category);
        })->withCount('comments')->where('status','Опубликовано')->first();
        $comments = Comments::where('post_id', $post->id)->get();

        return view('single', compact('post', 'comments'));
    }

    /**
     * Поиск
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {

        $search = $request->input('search');

        $posts = Post::where('title','LIKE','%'.$search.'%')
            ->orwhere('text','LIKE','%'.$search.'%')
            ->with('category')
            ->get();
//            ->paginate(10);

        return $search == 'Найти..' ? redirect()->back() : view('search', compact('posts', 'search'));

    }

    public function comment(Request $request, $id)
    {
        $data = $request->all();
        $data['post_id'] = $id;
        Comments::create($data);
        return redirect()->back();

    }

}
