<?php

namespace App\Http\Controllers;

use App\Mail\FeedbackMail;
use App\Models\Category;
use App\Models\Feedback;
use App\Models\Newsletter;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $categoryTabs = [];
        foreach ($categories as $category) {
            $categoryTabs[] = Post::with('category')->whereHas('category', function($query) use ($category) {
                $query->where('name', $category->name);
            })->inRandomOrder()->limit(3)->where('status','Опубликовано')->get();
        }
        $posts = Post::latest('created_at')->with('category')->where('status','Опубликовано')->limit(6)->get();
        return view('home.index', compact('categoryTabs', 'posts'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function gallery()
    {
        $posts = Post::latest('created_at')->with('category')->where('status','Опубликовано')->get();
        return view('gallery', compact('posts'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        return view('contact');
    }

    public function feedback(Request $request)
    {
        $data = $request->all();
        $feedback = Feedback::create($data);

        $user = User::first();
        Mail::to($user->email)->send(new FeedbackMail($feedback, $user));


        return redirect()->back();
    }

    public function newsletter(Request $request)
    {
        $data = $request->all();
        Newsletter::create($data);

        return redirect()->route('index');
    }
}
