<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\MenuItem;
use App\Models\Post;
use App\Models\Social;
use App\Models\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Info;
use App\Models\Menu;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $info;
    protected $menu;
    protected $gallery;
    protected $user;
    protected $topPosts;
    protected $categories;
    protected $social;

    public function __construct()
    {
        $this->social = Social::all();
        $this->info = Info::first();
        $this->menu = MenuItem::with('page','category')->orderBy('lft')->get();
        $this->user = User::first();
        $this->gallery = Post::with('category')->inRandomOrder()->limit(9)->where('status','Опубликовано')->get();
        $this->topPosts = Post::with('category')->withCount('comments')->with('comments')->orderBy('comments_count','DESC')->limit(5)->where('status','Опубликовано')->get();
        $this->categories = Category::inRandomOrder()->limit(3);
        \View::share([
            'info'     => $this->info, 'menu' => $this->menu,
            'gallery'  => $this->gallery, 'user' => $this->user,
            'topPosts' => $this->topPosts, 'categories' => $this->categories,
            'social' =>$this->social
        ]);
    }
}
