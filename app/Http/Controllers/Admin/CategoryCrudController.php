<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CategoryRequest as StoreRequest;
use App\Http\Requests\CategoryRequest as UpdateRequest;

class CategoryCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Category');
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin').'/category');
        $this->crud->setEntityNameStrings('категорию', 'Категории');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Название',
        ]);

        $this->crud->addColumn([
            'name' => 'slug',
            'label' => 'URL',
        ]);


        // ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
            'name' => 'name',
            'label' => 'Название',
            'type' => 'text',
            'placeholder' => 'Введите название',
        ]);
        $this->crud->addField([
            'name' => 'slug',
            'label' => 'URL',
            'type' => 'text',
            'hint' => 'Будет автоматически сгенерирован из заголовка, если это поле останется пустым',
            // 'disabled' => 'disabled'
        ]);


        $this->crud->addField([    // WYSIWYG
            'name' => 'description',
            'label' => 'Описание',
            'type' => 'ckeditor',
            'placeholder' => 'Описание',
        ]);


    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
