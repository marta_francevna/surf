<?php

namespace App\Http\Controllers\Admin;

// VALIDATION: change the requests to match your own file names if you need form validation
use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\PagesRequest as StoreRequest;
use App\Http\Requests\PagesRequest as UpdateRequest;


class PagesCrudController extends CrudController
{
    public function setup()
    {
        parent::__construct();

        $modelClass = config('backpack.pagemanager.page_model_class', 'Backpack\PageManager\app\Models\Page');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel($modelClass);
        $this->crud->setRoute(config('backpack.base.route_prefix').'/page');
        $this->crud->setEntityNameStrings('страницу', 'Страницы');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Название',
        ]);

        $this->crud->addColumn([
            'name' => 'slug',
            'label' => 'URL',
        ]);

        $this->crud->addButtonFromModelFunction('line', 'open', 'getOpenButton', 'beginning');

    }

    // -----------------------------------------------
    // Overwrites of CrudController
    // -----------------------------------------------

    // Overwrites the CrudController create() method to add template usage.
    public function create()
    {
        $this->addDefaultPageFields();

        return parent::create();
    }

    // Overwrites the CrudController store() method to add template usage.
    public function store(StoreRequest $request)
    {
        $this->addDefaultPageFields();

        return parent::storeCrud();
    }

    // Overwrites the CrudController edit() method to add template usage.
    public function edit($id)
    {
        $this->addDefaultPageFields();

        return parent::edit($id);
    }

    // Overwrites the CrudController update() method to add template usage.
    public function update(UpdateRequest $request)
    {
        $this->addDefaultPageFields();

        return parent::updateCrud();
    }

    // -----------------------------------------------
    // Methods that are particular to the PageManager.
    // -----------------------------------------------

    public function addDefaultPageFields()
    {

        $this->crud->addField([
            'name' => 'name',
            'label' => 'Название (только для администратора)',
            'type' => 'text',
            // 'disabled' => 'disabled'
        ]);
        $this->crud->addField([
            'name' => 'title',
            'label' => 'Заголовок',
            'type' => 'text',
            // 'disabled' => 'disabled'
        ]);
        $this->crud->addField([
            'name' => 'slug',
            'label' => 'URL',
            'type' => 'text',
            'hint' => "Будет автоматически сгенерирован из заголовка, если это поле останется пустым",
            // 'disabled' => 'disabled'
        ]);
        $this->crud->addField([
            'name' => 'content',
            'label' => "Контент страницы",
            'type' => 'wysiwyg'
        ]);
    }

}
