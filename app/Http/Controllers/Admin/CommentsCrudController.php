<?php

namespace App\Http\Controllers\Admin;

use App\Models\Newsletter;
use App\Notifications\PostNotification;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PostRequest as StoreRequest;
use App\Http\Requests\PostRequest as UpdateRequest;

class CommentsCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Comments');
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin').'/comments');
        $this->crud->setEntityNameStrings('Комментарии', 'Комментарии');

        $this->crud->addColumn([
            'name'  => 'name',
            'label' => 'Имя',
        ]);

        $this->crud->addColumn([
            'label' => 'Пост',
            'type' => 'select',
            'name' => 'post_id',
            'entity' => 'post',
            'attribute' => 'title',
            'model' => "App\Models\Post",
        ]);

        $this->crud->addColumn([
            'name'  => 'message',
            'label' => 'Комментарий',
        ]);

        $this->crud->removeButton('create');
        $this->crud->removeButton('update');
        $this->crud->addButtonFromModelFunction('line', 'open', 'getOpenButton', 'beginning');
    }

}
