<?php

namespace App\Http\Controllers\Admin;

use App\Models\Newsletter;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PostRequest as StoreRequest;
use App\Http\Requests\PostRequest as UpdateRequest;

class FeedbackCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Feedback');
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin').'/feedback');
        $this->crud->setEntityNameStrings('Обратная связь', 'Обратная связь');

        $this->crud->addColumn([
            'name'  => 'name',
            'label' => 'Имя',
        ]);

        $this->crud->addColumn([
            'name'  => 'email',
            'label' => 'Email',
        ]);

        $this->crud->addColumn([
            'name'  => 'message',
            'label' => 'Сообщение',
        ]);

        $this->crud->removeButton('create');
        $this->crud->removeButton('update');
    }

}
