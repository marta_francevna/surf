<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\SocialRequest as StoreRequest;
use App\Http\Requests\SocialRequest as UpdateRequest;

class SocialCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Social');
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin').'/social');
        $this->crud->setEntityNameStrings('cоциальную сеть', 'Социальные сети');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumn([
            'name' => 'icon',
            'label' => 'Иконка',
        ]);

        $this->crud->addColumn([
            'name' => 'link',
            'label' => 'Ссылка',
        ]);

        $this->crud->addField([
            'name' => 'link',
            'label' => 'Ссылка',
            'type' => 'text',
        ]);

        $this->crud->addField([
            'label'   => 'Icon',
            'name'    => 'icon',
            'type'    => 'icon_picker',
            'iconset' => 'fontawesome',
        ]);


    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
