<?php

namespace App\Http\Controllers\Admin;


use Backpack\CRUD\app\Http\Controllers\CrudController;


class NewsletterCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Newsletter');
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin').'/newsletter');
        $this->crud->setEntityNameStrings('Подписка', 'Подписка');

        $this->crud->addColumn([
            'name'  => 'name',
            'label' => 'Имя',
        ]);

        $this->crud->addColumn([
            'name'  => 'email',
            'label' => 'Email',
        ]);

        $this->crud->removeButton('create');
        $this->crud->removeButton('update');
    }

}
