<?php

namespace App\Http\Controllers\Admin;

use App\Models\MenuItem;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\MenuRequest as StoreRequest;
use App\Http\Requests\MenuRequest as UpdateRequest;

class MenuItemCrudController extends CrudController
{

    public function __construct()
    {
        parent::__construct();
        $this->crud->setModel("App\Models\MenuItem");
        $this->crud->setRoute(config('backpack.base.route_prefix').'/menu-item');
        $this->crud->setEntityNameStrings('меню', 'Меню');
        $this->crud->allowAccess('reorder');
        $this->crud->enableReorder('name', 1);
        $menu = MenuItem::all();
        if(count($menu)>8){
            $this->crud->removeButton('create');
        }
        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Название',
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => 'Название',
        ]);

        $this->crud->addField([
            'name' => 'type',
            'label' => 'Тип',
            'type' => 'page_or_link',
            'page_model' => '\App\Models\Page',
            'category_model' => '\App\Models\Category'
        ]);

    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud($request);
    }
    public function update(UpdateRequest $request)
    {
        return parent::updateCrud($request);
    }
}
