<?php

namespace App\Http\Controllers\Admin;

use App\Models\Info;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\InfoRequest as UpdateRequest;

class InfoCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        $this->crud->setModel('App\Models\Info');
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin').'/info');
        $this->crud->setEntityNameStrings('сайт', 'Сайт');
        $this->crud->addField([    // TEXT
            'name' => 'name',
            'label' => 'Название',
            'type' => 'text',
            'placeholder' => 'Введите название сайта',
            'tab'   => 'Главные настройки',
        ]);

        $this->crud->addField([
            'name' => 'logo',
            'label' => 'Логотип',
            'type' => 'image',
            'upload' => true,
            'crop' => true,
            'prefix' => 'images/',
             'tab'   => 'Главные настройки',
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'slider_name',
            'label' => 'Название на главном изображении',
            'type' => 'text',
            'placeholder' => 'Название на главном изображении',
            'tab'   => 'Главные настройки',
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'quote',
            'label' => 'Читата',
            'type' => 'text',
            'placeholder' => 'Цитата',
            'tab'   => 'Главные настройки',
        ]);

        $this->crud->addField([
            'name' => 'image_slider',
            'label' => 'Главное изображение',
            'type' => 'image',
            'upload' => true,
            'crop' => true,
            'prefix' => 'images/',
            'tab'   => 'Главные настройки',
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'desc_slider',
            'label' => 'Описание сайта',
            'type' => 'text',
            'placeholder' => 'Введите текст',
            'tab'   => 'Описания разделов',
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'desc_gallery',
            'label' => 'Описание галлереи',
            'type' => 'text',
            'placeholder' => 'Введите текст',
            'tab'   => 'Описания разделов',
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'desc_contact',
            'label' => 'Описание страницы контактов',
            'type' => 'text',
            'placeholder' => 'Введите текст',
            'tab'   => 'Описания разделов',
        ]);



    }

    public function edit($id = 1)
    {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);

    }


    public function update(UpdateRequest $request)
    {
        $this->crud->hasAccessOrFail('update');

        // fallback to global request instance
        if (is_null($request)) {
            $request = \Request::instance();
        }

        // replace empty values with NULL, so that it will work with MySQL strict mode on
        foreach ($request->input() as $key => $value) {
            if (empty($value) && $value !== '0') {
                $request->request->set($key, null);
            }
        }

        // update the row in the db
        $item = $this->crud->update($request->get($this->crud->model->getKeyName()),
            $request->except('save_action', '_token', '_method'));
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        // save the redirect choice for next time
        $this->setSaveAction();

        return  \Redirect::to('/admin/settings');

    }

}
