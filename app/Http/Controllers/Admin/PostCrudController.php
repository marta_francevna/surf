<?php

namespace App\Http\Controllers\Admin;

use App\Mail\PostMail;
use App\Models\Newsletter;
use App\Models\Post;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PostRequest as StoreRequest;
use App\Http\Requests\PostRequest as UpdateRequest;
use Illuminate\Support\Facades\Mail;

class PostCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Post');
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin').'/post');
        $this->crud->setEntityNameStrings('пост', 'Посты');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
        */

        $this->crud->addColumn([
            'name' => 'created_at',
            'label' => 'Дата',
            'type' => 'date'
        ]);

        $this->crud->addColumn([
            'name' => 'image',
            'label' => 'Изображение',
            'type' => 'image',
            'prefix' => 'images/posts/',
             'height' => '50px',
             'width' => '50px',
             'upload' => true,
        ]);

        $this->crud->addColumn([
            'name' => 'title',
            'label' => 'Заголовок',
        ]);

        $this->crud->addColumn([
            'name' => 'status',
            'label' => 'Статус',
        ]);


        $this->crud->addColumn([
            'label' => 'Категория',
            'type' => 'select',
            'name' => 'category_id',
            'entity' => 'category',
            'attribute' => 'name',
            'model' => "App\Models\Category",
        ]);

        // ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
            'name' => 'title',
            'label' => 'Заголовок',
            'type' => 'text',
            'placeholder' => 'Введите заголовок',
        ]);
        $this->crud->addField([
            'name' => 'slug',
            'label' => 'URL',
            'type' => 'text',
            'hint' => 'Будет автоматически сгенерирован из заголовка, если это поле останется пустым',
            // 'disabled' => 'disabled'
        ]);

        $this->crud->addField([    // WYSIWYG
            'name' => 'text',
            'label' => 'Контент',
            'type' => 'ckeditor',
            'placeholder' => 'Текст поста',
        ]);
        $this->crud->addField([    // Image
            'name' => 'image',
            'label' => 'Изображение',
            'type' => 'image',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
//            'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
             'prefix' => 'images/posts/'
        ]);

        $this->crud->addField([
            'label' => 'Категория',
            'type' => 'select',
            'name' => 'category_id',
            'entity' => 'category',
            'attribute' => 'name',
            'model' => "App\Models\Category",
        ]);


        $this->crud->addField([    // ENUM
            'name' => 'status',
            'label' => 'Status',
            'type' => 'enum',
        ]);

        $this->crud->enableAjaxTable();
    }

    public function store(StoreRequest $request)
    {
        $response = parent::storeCrud();
        $id = $this->crud->entry->id;
        $post = Post::find($id);
        $users = Newsletter::all();
        $admin = User::first();
        foreach ($users as $user){
            Mail::to($user->email)->send(new PostMail($post, $admin));
        }

        return $response;
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
