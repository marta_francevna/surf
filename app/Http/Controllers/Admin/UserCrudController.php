<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\UserRequest as UpdateRequest;

class UserCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        $this->crud->setModel('App\Models\User');
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin').'/user');
        $this->crud->setEntityNameStrings('владельца', 'Владелец');

        $this->crud->addField([    // TEXT
            'name' => 'phone',
            'label' => 'Телефон',
            'type' => 'text',
            'placeholder' => 'Введите телефон',
        ]);

        $this->crud->addField([
            'name' => 'image',
            'label' => 'Фото',
            'type' => 'image',
            'upload' => true,
            'crop' => true,
            'prefix' => 'images/',
        ]);


        $this->crud->addField([    // TEXT
            'name' => 'about',
            'label' => 'О себе',
            'type' => 'text',
            'placeholder' => 'О себе',
        ]);


    }

    public function edit($id = 1)
    {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);

    }


    public function update(UpdateRequest $request)
    {
        $this->crud->hasAccessOrFail('update');

        // fallback to global request instance
        if (is_null($request)) {
            $request = \Request::instance();
        }

        // replace empty values with NULL, so that it will work with MySQL strict mode on
        foreach ($request->input() as $key => $value) {
            if (empty($value) && $value !== '0') {
                $request->request->set($key, null);
            }
        }

        // update the row in the db
        $item = $this->crud->update($request->get($this->crud->model->getKeyName()),
            $request->except('save_action', '_token', '_method'));
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        // save the redirect choice for next time
        $this->setSaveAction();

        return  \Redirect::to('/admin/user');

    }

}
