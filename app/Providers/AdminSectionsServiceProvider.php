<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Feedback;
use App\Models\Info;
use App\Models\Menu;
use App\Models\Newsletter;
use App\Models\Page;
use App\Models\Post;
use App\Models\User;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        Category::class => 'App\Http\Sections\Category',
        Feedback::class => 'App\Http\Sections\Feedback',
        Info::class => 'App\Http\Sections\Info',
        Menu::class => 'App\Http\Sections\Menu',
        Newsletter::class => 'App\Http\Sections\Newsletter',
        Page::class => 'App\Http\Sections\Page',
        Post::class => 'App\Http\Sections\Post',
        User::class => 'App\Http\Sections\User',
    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
    }
}
