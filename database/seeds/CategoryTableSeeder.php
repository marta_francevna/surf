<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Категория',
            'slug' => 'kategory1',
            'description' => 'Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable, Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable',
        ]);
        Category::create([
            'name' => 'Категория',
            'slug' => 'kategory2',
            'description' => 'Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable, Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable',
        ]);
        Category::create([
            'name' => 'Категория',
            'slug' => 'kategory3',
            'description' => 'Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable, Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable',
        ]);
    }
}
