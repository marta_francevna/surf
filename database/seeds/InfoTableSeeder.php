<?php

use Illuminate\Database\Seeder;
use App\Models\Info;

class InfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Info::create([
            'name' => 'Surf-blog',
            'logo' => 'logo.png',
            'quote' => 'Просто, уникально, надёжно, функционально.',
            'image_slider' => 'banner-1.jpg',
            'slider_name' => 'S.U.R.F CMS',
            'desc_slider' => 'Для создания блога на S.U.R.F CMS потребуется несколько часов и не нужно тратить огромные ресурсы на составление плана архитектуры сайта, планировать базу данных для сайта, разрабатывать дизайн',
            'desc_gallery' => 'Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable, Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable',
            'desc_contact' => 'Nemo enim ips voluptatem voluptas sitsper natuaut odit aut fugit consequuntur magni dolores eosqratio nevoluptatem amet eism com odictor ut ligulate cot ameti dapibu',
          ]);
    }
}
