<?php

use Illuminate\Database\Seeder;
use App\Models\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create( [
            'name'       => 'Marta',
            'email'      => 'marta@webempire.by',
            'password'   => bcrypt( 'secret' ),
            'phone'      => '+375(29)5665078',
            'image'      => 't4.jpg',
            'about'      => 'Consectetur adipisicing Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod .',
        ] );
    }
}
