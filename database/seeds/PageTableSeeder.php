<?php

use Illuminate\Database\Seeder;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Page::create([
            'name' => 'Разработчик',
            'title' => 'Разработчик',
            'slug' => 'about',
            'content' => '<div class="welcome-bottom">
                                <img src="/images/t4.jpg" class="img-responsive" alt="">
                                <p>Vivamus interdum diam diam, non faucibus tortor consequat vitae. Proin sit amet augue sed massa pellentesque viverra. Suspendisse iaculis purus eget est pretium aliquam ut sed diam. Nullam non magna lobortis, faucibus erat eu, consequat justo. Suspendisse commodo nibh odio, vel elementum nulla luctus sit amet.</p>
                                <p>Nulla in tempor lectus. Etiam ac mauris lacinia nulla ultricies porta sit amet eleifend ligula. Quisque tincidunt vitae turpis at efficitur. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec sagittis, magna a sagittis dapibus, ipsum metus interdum lectus, quis feugiat leo ipsum nec diam.</p>
                            </div>'
        ]);

        \App\Models\Page::create([
            'name' => 'Тестовая страница',
            'title' => 'Тестовая страница',
            'slug' => 'test',
            'content' => ' <div class="features-main">
                        <div class="fea-top">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta.</p>
                        </div>
                        <div class="feature-botttom">
                            <div class="col-md-6 fea-grid">
                                <div class="fea-img">
                                    <img src="/images/f1.jpg" alt="">
                                </div>
                                <div class="fea-text">
                                    <h4>Et iusto odio dignissimos</h4>
                                    <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="col-md-6 fea-grid">
                                <div class="fea-img">
                                    <img src="/images/m1.jpg" alt="">
                                </div>
                                <div class="fea-text">
                                    <h4>Ducimus odio dignissimos</h4>
                                    <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                            </div>
                            <div class="col-md-6 fea-grid">
                                <div class="fea-img">
                                    <img src="/images/t2.jpg" alt="">
                                </div>
                                <div class="fea-text">
                                    <h4>Et iusto odio dignissimos</h4>
                                    <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="col-md-6 fea-grid">
                                <div class="fea-img">
                                    <img src="/images/t3.jpg" alt="">
                                </div>
                                <div class="fea-text">
                                    <h4>Ducimus odio dignissimos</h4>
                                    <p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                </div>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>'
        ]);

    }
}
