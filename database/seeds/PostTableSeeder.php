<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Post;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 10) as $index => $value) {

            Post::create([
                'category_id' => 1,
                'title' => $faker->text(20),
                'slug'  => $faker->slug,
                'image' => $faker->image(public_path('images/posts'), 750, 450, 'food', false),
                'text'  => $faker->text(500),
            ]);
        }

        foreach (range(1, 10) as $index => $value) {

            Post::create([
                'category_id' => 2,
                'title' => $faker->text(20),
                'slug'  => $faker->slug,
                'image' => $faker->image(public_path('images/posts'), 750, 450, 'cats', false),
                'text'  => $faker->text(500),
            ]);

            Post::create([
                'category_id' => 3,
                'title' => $faker->text(20),
                'slug'  => $faker->slug,
                'image' => $faker->image(public_path('images/posts'), 750, 450, 'nightlife', false),
                'text'  => $faker->text(500),
            ]);
        }
    }
}
