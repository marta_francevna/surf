<?php

use Illuminate\Database\Seeder;
use Backpack\MenuCRUD\app\Models\MenuItem;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MenuItem::create([
            'name' => 'Галлерея',
            'link' => 'gallery',
            'type' => 'internal_link',
        ]);

        MenuItem::create([
            'name' => 'Категория',
            'type' => 'category_link',
            'category_id' => 1
        ]);
        MenuItem::create([
            'name' => 'Категория',
            'type' => 'category_link',
            'category_id' => 2
        ]);
        MenuItem::create([
            'name' => 'Категория',
            'type' => 'category_link',
            'category_id' => 3
        ]);
        MenuItem::create([
            'name' => 'Разработчик',
            'type' => 'page_link',
            'page_id'=> 1
        ]);
        MenuItem::create([
            'name' => 'Тестовая страница',
            'type' => 'page_link',
            'page_id'=> 2
        ]);
        MenuItem::create([
            'name' => 'Контакты',
            'link' => 'contact',
            'type' => 'internal_link',
        ]);
    }
}
