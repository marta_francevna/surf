<?php

use Illuminate\Database\Seeder;
use App\Models\Comments;
use Faker\Factory as Faker;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        foreach (range(1, 5) as $index => $value) {

            Comments::create([
                'post_id' => '1',
                'name'    => $faker->name,
                'message' => $faker->text(50)
            ]);
        }

        foreach (range(1, 3) as $index => $value) {

            Comments::create([
                'post_id' => '2',
                'name'    => $faker->name,
                'message' => $faker->text(50)
            ]);
        }

            Comments::create([
                'post_id' => '3',
                'name'    => $faker->name,
                'message' => $faker->text(50)
            ]);

    }
}
