<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('logo')->default('logo.png');
            $table->string('quote');
            $table->string('image_slider')->default('banner-1.jpg');
            $table->string('slider_name')->nullable();
            $table->text('desc_slider')->nullable();
            $table->text('desc_gallery')->nullable();
            $table->text('desc_contact')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info');
    }
}
